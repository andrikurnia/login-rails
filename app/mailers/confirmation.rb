class Confirmation < ActionMailer::Base
  default from: "from@example.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.confirmation.register.subject
  #
  def register(email)
    @email = email

    mail to: email, subject: "E-mail Confirmation"
  end
end
