class User < ActiveRecord::Base
  validates :username, :email, presence: true, uniqueness: true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }

	has_many :posts, dependent: :destroy
	has_many :channels, dependent: :destroy
  has_secure_password

  def is_admin?
    self.role_id == 2
  end
  
end
