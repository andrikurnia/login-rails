class Post < ActiveRecord::Base
	belongs_to :channel
	belongs_to :user
	has_many :thread_post, class_name: "Post",
                          foreign_key: "thread_id"
  	belongs_to :thread, class_name: "Post"
end
