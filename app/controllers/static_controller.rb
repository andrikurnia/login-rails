class StaticController < ApplicationController
	skip_before_action :authorize

  def index
    if params[:set_locale]
      redirect_to root_url(locale: params[:set_locale])
    else
    	@forums = Forum.all
    	@users = User.all
    end
  end
end
