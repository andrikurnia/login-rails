class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :set_i18n_locale_from_params
  protect_from_forgery with: :exception
  before_action :authorize

  protected
  	def authorize
  		unless User.find_by(id: session[:user_id])
  			redirect_to login_url, alert: "You must login"
  		end
  	end

    def authorize_admin
      authorize
      admin = User.find_by(id: session[:user_id])
      unless admin.role_id == 2
        redirect_to root_url
      end
    end

  	def already_login
  		unless session[:user_id].nil?
  			redirect_to admin_url, alert: "Sorry, You don't have permission to acccess ", :flash => {:url => @_env["REQUEST_URI"]}
  		end
  	end

    def set_i18n_locale_from_params
      if params[:locale]
        if I18n.available_locales.map(&:to_s).include?(params[:locale])
          I18n.locale = params[:locale]
        else
          flash.now[:notice] = "#{params[:locale]} translation not avaiable"
          logger.error flash.now[:notice]
        end
      end
    end

    def default_url_options
      { locale: I18n.locale }
    end
end
