class SessionsController < ApplicationController
	skip_before_action :authorize
	before_action :already_login, only: [:new]

  def new
  end

  def create
  	user = User.find_by(:username => params[:username])
  	if user and user.authenticate(params[:password])
  		session[:user_id] = user.id
      if user.is_admin?
        redirect_to admin_url
      else
        redirect_to root_url
  	  end
    else
  		redirect_to login_url, alert: 'Sorry, Your username or password seems legit'
  	end
  end

  def destroy
  	session[:user_id] = nil
  	redirect_to root_url
  end
end
