class AdminController < ApplicationController
	layout 'admin'
  before_action 'authorize_admin'
  def index
  	
  end

  def users
  	@users = User.all
  end

  def forums
  	@forums = Forum.all
  end
  
end
