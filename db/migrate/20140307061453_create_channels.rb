class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :title
      t.text :body
      t.integer :counter
      t.belongs_to :forum
      t.belongs_to :user
      t.timestamps
    end
  end
end
