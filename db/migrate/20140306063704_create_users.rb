class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password_digest
      t.string :email
      t.belongs_to :role, :default => 1

      t.timestamps
    end

    create_table :roles do |t|
      t.string :role
    end
  end
end
