# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# User.create(username: 'admin', password_digest: '$2a$10$6IBRWHep50XGlF3Asq0j4uUAjBshLHJ0HCVUIsiawd3qZYXzeEjrK', email: 'admin@tesseract.com', role_id: 2)
ActiveRecord::Base.connection.execute "insert into roles values (null, 'user'), (null, 'admin')"
ActiveRecord::Base.connection.execute "insert into users values (null, 'admin', '$2a$10$6IBRWHep50XGlF3Asq0j4uUAjBshLHJ0HCVUIsiawd3qZYXzeEjrK', 'admin@tesseract.com', '2', '1995-03-14 04:00:00', '1995-03-14 04:00:00')"
